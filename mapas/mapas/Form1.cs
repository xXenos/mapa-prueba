﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GMap.NET;
using GMap.NET.MapProviders;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;

namespace mapas
{
    public partial class Form1 : Form
    {
        GMarkerGoogle marker;
        GMapOverlay markeroverlay;
        DataTable dt;

        //Ruta automatizada (direccion)
        bool trazartura = false;
        int contadorIndicadoresRuta = 0;
        PointLatLng Inicial;
        PointLatLng Final;

        int filaseleccionada = 0;
        double LatInicial = 14.976666;
        double LangInicial = -89.527511;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dt = new DataTable();
            dt.Columns.Add(new DataColumn("Descripcion", typeof(string)));
            dt.Columns.Add(new DataColumn("Latitud", typeof(double)));
            dt.Columns.Add(new DataColumn("Longitud", typeof(double)));

            //insertar un dato en el datagrid
            dt.Rows.Add("Ubicacion 1",LatInicial, LangInicial);
            dgv.DataSource = dt;
            //Desactivar las columas lat y long
            dgv.Columns[1].Visible = false;
            dgv.Columns[2].Visible = false;

            gMapControl1.DragButton = MouseButtons.Left;
            gMapControl1.CanDragMap = true;
            gMapControl1.MapProvider = GMapProviders.GoogleMap;
            gMapControl1.Position = new PointLatLng(LatInicial, LangInicial);
            gMapControl1.MinZoom = 0;
            gMapControl1.MaxZoom = 24;
            gMapControl1.Zoom = 9;
            gMapControl1.AutoScroll = true;

            //marcadores
            markeroverlay = new GMapOverlay("Marcador");
            marker = new GMarkerGoogle(new PointLatLng(LatInicial, LangInicial), GMarkerGoogleType.red);
            markeroverlay.Markers.Add(marker); //agregamos al mapa

            //agregamos un tootip de texto a los marcadores
            marker.ToolTipMode = MarkerTooltipMode.Always;
            marker.ToolTipText = string.Format("Ubicacion: \n Latitud:{0} \n Longitud: {1}", LatInicial, LangInicial);

            //agregar al mapa y el marcador el map control
            gMapControl1.Overlays.Add(markeroverlay);
        }

        private void seleccionarregistro(object sender, DataGridViewCellMouseEventArgs e)
        {
            filaseleccionada = e.RowIndex; //fila seleccionada
            //recuperar los datos del grid y los assignados
            txtdescripcion.Text = dgv.Rows[filaseleccionada].Cells[0].Value.ToString();
            txtlatitud.Text = dgv.Rows[filaseleccionada].Cells[1].Value.ToString();
            txtlongitud.Text = dgv.Rows[filaseleccionada].Cells[2].Value.ToString();
            //se asignan los valores del grid al marcador
            marker.Position = new PointLatLng(Convert.ToDouble(txtlatitud.Text), Convert.ToDouble(txtlongitud.Text));
            //se posiciona el foco de ese punto
            gMapControl1.Position = marker.Position;
        }

        private void gMapControl1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            // se obtiene los datos de lat y lng del mapa donde usuario presiono
            double lat = gMapControl1.FromLocalToLatLng(e.X, e.Y).Lat;
            double lng = gMapControl1.FromLocalToLatLng(e.X, e.Y).Lng;

            //se colocan los datos en los txt de la longitud y latitud
            txtlatitud.Text = lat.ToString();
            txtlongitud.Text = lng.ToString();
            
            //creacion del marcador para moverlo al lugar indicado
            marker.Position = new PointLatLng(lat, lng);
            //tambien agregar el mensaje (tooltip)
            marker.ToolTipText = string.Format("Ubicacion: \n Latitud: {0} \n Longitud: {1}", lat, lng);

            CrearDireccionTrazarRuta(lat, lng);
        }

        private void btnagregar_Click(object sender, EventArgs e)
        {
            dt.Rows.Add(txtdescripcion.Text, txtlatitud.Text, txtlongitud.Text);
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            dgv.Rows.RemoveAt(filaseleccionada); //remover de la tabla
            //procedimiento para eliminar de la base de datos
        }

        private void btnrutas_Click(object sender, EventArgs e)
        {
            GMapOverlay Ruta = new GMapOverlay("CapaRuta");
            List<PointLatLng> puntos = new List<PointLatLng>();
            //variables para almacenar datos
            double lng, lat;
            //agregamos los datos del grid
            for (int filas = 0; filas < dgv.Rows.Count; filas++)
            {
                lat = Convert.ToDouble(dgv.Rows[filas].Cells[1].Value);
                lng = Convert.ToDouble(dgv.Rows[filas].Cells[2].Value);
                puntos.Add(new PointLatLng(lat, lng));
            }
            GMapRoute PuntosRuta = new GMapRoute(puntos, "Rutas");
            Ruta.Routes.Add(PuntosRuta);
            gMapControl1.Overlays.Add(Ruta);
            //Actualizar el mapa
            gMapControl1.Zoom = gMapControl1.Zoom + 1;
            gMapControl1.Zoom = gMapControl1.Zoom - 1;
        }

        private void btnllegar_Click(object sender, EventArgs e)
        {
            trazartura = true;
            btnllegar.Enabled = false;
        }

        public void CrearDireccionTrazarRuta(double lat, double lng)
        {
            if (trazartura)
            {
                switch(contadorIndicadoresRuta){
                    case 0://primer marcador o inicial
                        contadorIndicadoresRuta++;
                        Inicial = new PointLatLng(lat,lng);
                        break;
                    case 1://segundo marcador o final
                        contadorIndicadoresRuta++;
                        Final = new PointLatLng(lat, lng);
                        GDirections direccion;
                        var RutasDireccion = GMapProviders.GoogleMap.GetDirections(out direccion, Inicial, Final, false, false, false, false, false);
                        GMapRoute RutaObtenida = new GMapRoute(direccion.Route, "Ruta Ubicacion");
                        GMapOverlay CapaRuta = new GMapOverlay("Capa de la ruta");
                        CapaRuta.Routes.Add(RutaObtenida);
                        gMapControl1.Overlays.Add(CapaRuta);
                        gMapControl1.Zoom = gMapControl1.Zoom + 1;
                        gMapControl1.Zoom = gMapControl1.Zoom - 1;
                        trazartura = false;
                        btnagregar.Enabled = true;
                        break;
                }
            }
        }
    }
}
